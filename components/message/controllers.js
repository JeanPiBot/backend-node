const store = require('./store');
const connection = require('./model');

const addUser = (user, message) => {
    return new Promise((resolve, reject) => {
        if(!user || !message) {
            console.error(`Message controller no hay usuario o mensaje`);
            reject('Los datos son incorrectos');
            return  false;
        }
        const fullMessage = {
            user: user,
            message: message,
            date: new Date(),
        }

        store.add(fullMessage, connection);
        resolve(fullMessage);
    });
}

const getMessage = () => {
    return new Promise((resolve, reject) => {
        if (store.list(connection) === '') {
            reject('No hay datos');
        }
        resolve(store.list());
    })
}

module.exports = {
    addUser,
    getMessage,
};